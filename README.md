# Mobiquity Demo

## How it was implemented
- `Maven` was used to build application
- `WebClient` was used for integration with `https://www.ing.nl/api/locator/atms/`
- Controller layer: `AtmJsonController` which will provide API and `AtmHtmlController` which will provide access to views that were create in Spring Thymeleaf
- `IngRepository` is responsible for getting information from `https://www.ing.nl/api/locator/atms/`
- Service layer is responsible for filtering operations

### Use credentials in login form:
- Username: `user`
- Password: `password`
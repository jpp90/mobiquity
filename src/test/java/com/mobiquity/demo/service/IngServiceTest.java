package com.mobiquity.demo.service;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import com.mobiquity.demo.dto.ing.AddressDto;
import com.mobiquity.demo.dto.ing.AtmDto;
import com.mobiquity.demo.repository.IngRepository;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IngServiceTest {

  @Mock
  private IngRepository ingRepository;

  @InjectMocks
  private IngService ingService;

  @Nested
  class FindBy {

    @Test
    public void shouldReturnFiveAtmsForGivenCity() {
      given(ingRepository.getAllAtms()).willReturn(createListOfAtms());

      List<AtmDto> atms = ingService.findBy("amsterdam");

      assertThat(atms).hasSize(2);
      assertThat(atms.get(0).getAddress().getStreet()).isEqualTo("Vaart ZZ");
      assertThat(atms.get(1).getAddress().getStreet()).isEqualTo("Hugo de Grootplein");
    }

    @Test
    public void shouldReturnEmptyList() {
      given(ingRepository.getAllAtms()).willReturn(emptyList());

      List<AtmDto> atms = ingService.findBy("Amsterdam");

      assertThat(atms).hasSize(0);
    }
  }

  @Nested
  class FindAllCities {

    @Test
    public void shouldReturnFiveAtmsWithoutRepetitions() {
      given(ingRepository.getAllAtms()).willReturn(createListOfAtms());

      List<String> atms = ingService.findAllCities();

      assertThat(atms).hasSize(5);
    }

    @Test
    public void shouldReturnEmptyList() {
      given(ingRepository.getAllAtms()).willReturn(emptyList());

      List<String> atms = ingService.findAllCities();

      assertThat(atms).hasSize(0);
    }
  }

  private ArrayList<AtmDto> createListOfAtms() {
    return Lists.newArrayList(
        buildAtmWithCity("Amsterdam", "Vaart ZZ"),
        buildAtmWithCity("AMSTERDAM", "Hugo de Grootplein"),
        buildAtmWithCity("Arnhem", "Oostburgwal"),
        buildAtmWithCity("ARNHEM", "Kronenburgpassage"),
        buildAtmWithCity("Sneek", "Sint Antoniusplein"),
        buildAtmWithCity("Hattem", "Vechtstraat"),
        buildAtmWithCity("Laren", "Huenderstraat"));
  }

  private AtmDto buildAtmWithCity(String city, String street) {
    return AtmDto.builder()
        .address(AddressDto.builder()
            .street(street)
            .city(city)
            .build())
        .build();
  }
}
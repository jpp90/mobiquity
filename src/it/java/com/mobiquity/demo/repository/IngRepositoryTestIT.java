package com.mobiquity.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.mobiquity.demo.dto.ing.AtmDto;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class IngRepositoryTestIT {

  @Autowired
  private IngRepository ingRepository;

  @Test
  public void shouldReturnListOfAtms() {
    List<AtmDto> atms = ingRepository.getAllAtms();

    assertThat(atms).hasSize(2749);
  }
}
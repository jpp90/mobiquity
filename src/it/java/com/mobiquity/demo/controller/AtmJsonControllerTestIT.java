package com.mobiquity.demo.controller;

import static com.mobiquity.demo.util.JsonHelper.readJsonFromIng;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiquity.demo.dto.ing.AtmDto;
import com.mobiquity.demo.repository.IngRepository;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class AtmJsonControllerTestIT {

  @MockBean
  private IngRepository ingRepository;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void shouldReturnListOfAtms() throws Exception {
    given(ingRepository.getAllAtms()).willReturn(atms());

    mockMvc.perform(get("/api/atms/{city}", "amsterdam")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].address.city").value("Amsterdam"))
        .andExpect(jsonPath("$[1].address.city").value("Amsterdam"));
  }

  @SneakyThrows
  private List<AtmDto> atms() {
    return objectMapper.readValue(readJsonFromIng(), new TypeReference<List<AtmDto>>() {
    });
  }
}
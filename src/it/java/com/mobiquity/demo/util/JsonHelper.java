package com.mobiquity.demo.util;

import java.nio.file.Files;
import lombok.SneakyThrows;
import org.springframework.util.ResourceUtils;

public class JsonHelper {

  @SneakyThrows
  public static String readJsonFromIng() {
    return new String(
        Files.readAllBytes(
            ResourceUtils.getFile("classpath:data/json/simple-response-from-ing.json").toPath()));
  }

  @SneakyThrows
  public static String readJsonWithAdditionalCharacters() {
    return new String(
        Files.readAllBytes(
            ResourceUtils
                .getFile("classpath:data/json/simple-response-with-additional-characters.json")
                .toPath()));
  }
}
package com.mobiquity.demo.service;

import static java.util.stream.Collectors.toList;

import com.mobiquity.demo.dto.ing.AtmDto;
import com.mobiquity.demo.repository.IngRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IngService {

  private final IngRepository ingRepository;

  public List<AtmDto> findBy(String city) {
    return getAllAtms()
        .stream()
        .filter(atmDto -> atmDto.getAddress().getCity().equalsIgnoreCase(city))
        .collect(toList());
  }

  public List<String> findAllCities() {
    return getAllAtms()
        .stream()
        .map(atmDto -> atmDto.getAddress().getCity().toUpperCase())
        .distinct()
        .sorted()
        .collect(toList());
  }

  private List<AtmDto> getAllAtms() {
    return ingRepository.getAllAtms();
  }
}
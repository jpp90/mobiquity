package com.mobiquity.demo.dto.ing;

import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AtmDto {

  AddressDto address;
  int distance;
  List<OpeningHoursDto> openingHours;
  String functionality;
  Type type;

}

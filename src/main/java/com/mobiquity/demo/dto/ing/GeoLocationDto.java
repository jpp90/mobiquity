package com.mobiquity.demo.dto.ing;

import lombok.Value;

@Value
public class GeoLocationDto {

  double lat;
  double lng;

}

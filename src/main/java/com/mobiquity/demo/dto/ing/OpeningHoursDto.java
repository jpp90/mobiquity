package com.mobiquity.demo.dto.ing;

import java.util.List;
import lombok.Value;

@Value
public class OpeningHoursDto {

  int dayOfWeek;
  List<HoursDto> hours;

}

package com.mobiquity.demo.dto.ing;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AddressDto {

  String street;
  String housenumber;
  String postalcode;
  String city;
  GeoLocationDto geoLocation;

}

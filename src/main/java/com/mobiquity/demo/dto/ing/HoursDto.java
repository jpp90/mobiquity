package com.mobiquity.demo.dto.ing;

import lombok.Value;

@Value
public class HoursDto {

  String hourFrom;
  String hourTo;

}

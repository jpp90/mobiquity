package com.mobiquity.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfiguration {

  @Value("${external.api.ing.url}")
  private String ingApiUrl;

  @Bean
  public WebClient getWebClientBuilder() {
    return WebClient.builder()
        .baseUrl(ingApiUrl)
        .exchangeStrategies(exchangeStrategies())
        .build();
  }

  @Bean
  public ExchangeStrategies exchangeStrategies() {
    return ExchangeStrategies.builder()
    .codecs(configurer -> configurer
        .defaultCodecs()
        .maxInMemorySize(16 * 1024 * 1024))
    .build();
  }
}

package com.mobiquity.demo.controller;

import com.mobiquity.demo.dto.ing.AtmDto;
import com.mobiquity.demo.service.IngService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class AtmJsonController {

  private final IngService ingService;

  @GetMapping("/api/atms/{city}")
  public ResponseEntity<List<AtmDto>> findAtmsBy(@PathVariable String city) {
    return ResponseEntity.ok(ingService.findBy(city));
  }
}
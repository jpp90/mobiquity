package com.mobiquity.demo.controller;

import com.mobiquity.demo.dto.html.City;
import com.mobiquity.demo.service.IngService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
@AllArgsConstructor
public class AtmHtmlController {

  private final IngService ingService;

  @GetMapping("/cityForm")
  public String getCityForm(Model model) {
    model.addAttribute("cities", ingService.findAllCities());
    model.addAttribute("city", new City());
    return "cityForm";
  }

  @GetMapping("/atms")
  public String findAtmsBy(@ModelAttribute City city, Model model) {
    model.addAttribute("atms", ingService.findBy(city.getName()));
    return "atms";
  }
}
package com.mobiquity.demo.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobiquity.demo.dto.ing.AtmDto;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class IngRepository {

  private final WebClient webClient;

  private final ObjectMapper objectMapper;

  public List<AtmDto> getAllAtms() {
    Mono<String> atmsJson = webClient
        .get()
        .retrieve()
        .bodyToMono(String.class);

    return mapToJson(atmsJson.block().substring(5));
  }

  @SneakyThrows
  private List<AtmDto> mapToJson(String atmsJson) {
    return objectMapper.readValue(atmsJson, new TypeReference<List<AtmDto>>() {
    });
  }
}
